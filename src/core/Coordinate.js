const { format } = require('util');

class Coordinate {
  /**
   * Coordinate constructor
   * @param x
   * @param y
   */
  constructor({ x, y }) {
    this._x = x;
    this._y = y;
  }

  /**
   * Get Hash from coordinate.
   * This is useful as { a: 1 } !== { a: 1 } and we store coordinates in a Set
   * @returns {String}
   */
  get hash() {
    return format('x%sy%s', this._x, this._y);
  }
}

module.exports = Coordinate;
