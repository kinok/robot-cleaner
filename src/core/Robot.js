const _ = require('lodash');
const Coordinate = require('./Coordinate');

class Robot {
  /**
   * Constructor
   * @param numberOfCommands
   * @param startCoordinates
   * @param commands
   */
  constructor({ numberOfCommands, startCoordinates, commands }) {
    this._numberOfCommands = numberOfCommands;
    [this._x, this._y] = _.map(_.split(startCoordinates, ' '), _.parseInt);
    this._commands = this._parse(commands);
    this._cleaned = new Set();
    this._boundaries = {
      maxCommand: 10000,
      maxX: 100000,
      maxY: 100000,
      minX: -100000,
      minY: -100000,
    };
  }

  /**
   * Overrides boundaries
   * @param maxCommand
   * @param maxX
   * @param maxY
   * @param minX
   * @param minY
   */
  setBoundaries({
    maxCommand = 10000, maxX = 100000, maxY = 100000, minX = -100000, minY = -100000,
  }) {
    this._boundaries = {
      maxCommand,
      maxX,
      maxY,
      minX,
      minY,
    };
  }

  /**
   * Parse command string and return an Array of Objects
   * @param commands
   * @returns {Array}
   * @private
   */
  _parse(commands) {
    return _.map(commands, (command) => {
      const [direction, distance] = _.split(command, ' ');
      return {
        direction,
        distance,
      };
    });
  }

  /**
   * Start the robot
   */
  start() {
    this._init();
    const maxCommand = _.min([this._boundaries.maxCommand, this._numberOfCommands]);
    for (let i = 0; i < maxCommand && this._commands[i]; i += 1) {
      const { direction, distance } = this._commands[i];
      this._move(direction, distance);
    }
  }

  /**
   * Initialize first position
   * @private
   */
  _init() {
    const coordinate = new Coordinate({
      x: this._x,
      y: this._y,
    });

    this._clean(coordinate);
  }

  /**
   * Start moving sequence
   * @param direction
   * @param distance
   * @private
   */
  _move(direction, distance) {
    for (let i = 0; i < distance; i += 1) {
      switch (direction) {
        case 'E':
          if (this._x + 1 <= this._boundaries.maxX) {
            this._x += 1;
          }
          break;
        case 'W':
          if (this._x - 1 >= this._boundaries.minX) {
            this._x -= 1;
          }
          break;
        case 'N':
          if (this._y + 1 <= this._boundaries.maxY) {
            this._y += 1;
          }
          break;
        case 'S':
          if (this._y - 1 >= this._boundaries.minY) {
            this._y -= 1;
          }
          break;
      }

      const coordinate = new Coordinate({
        x: this._x,
        y: this._y,
      });

      this._clean(coordinate);
    }
  }

  /**
   * Cleans current coordinate
   * @param coordinate Coordinate
   */
  _clean(coordinate) {
    this._cleaned.add(coordinate.hash);
  }

  /**
   * Get robot output
   * @returns {number}
   */
  get output() {
    return this._cleaned.size;
  }
}

module.exports = Robot;
