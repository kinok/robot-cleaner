const Promise = require('bluebird');
const _ = require('lodash');
const { EOL } = require('os');

class StdinHelper {
  /**
   * Constructor
   */
  constructor() {
    this._buffer = [];
  }

  /**
   * Read from stdin
   * @returns {Promise}
   */
  async read() {
    return new Promise((resolve) => {
      process.stdin.on('data', (chunk) => {
        this._buffer.push(chunk);
      });

      process.stdin.on('end', () => resolve(_.split(this._buffer.toString(), EOL)));
    });
  }
}

module.exports = StdinHelper;
