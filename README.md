# Robot cleaner

## Prerequisite
* nodejs 8+
* npm 5.6+

## Getting started
* Clone this repository
* Install dependencies: `npm install`
* Launch robot:
```
$ cat examples/test-square-2x2.input | bin/robot 
 => Cleaned: 4
```
* Launch test: `npm test`
* Once test has been launch, you can check coverage report in `./coverage/index.html`