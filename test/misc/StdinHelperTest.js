const StdinHelper = require('../../src/misc/StdinHelper');
const stdin = require('mock-stdin').stdin();
const _ = require('lodash');
const { EOL } = require('os');
require('should');

describe('StdinHelper', () => {
  describe('.constructor', () => {
    it('should return an instance of StdinHelper', () => {
      const stdinHelper = new StdinHelper();
      stdinHelper.should.be.instanceof(StdinHelper);
    });
  });

  describe('.read', () => {
    it('should read lines from Stdin and returns it as a string', async () => {
      const string = `Hello
this
is 
a
test`;
      process.nextTick(() => {
        stdin.send(string);
        stdin.send(null); // that ends the stream
      });

      const stdinHelper = new StdinHelper();
      const response = await stdinHelper.read();

      response.should.eql(_.split(string, EOL));
    });
  });
});
