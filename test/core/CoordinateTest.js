const Coordinate = require('../../src/core/Coordinate');
require('should');

describe('Coordinate', () => {
  describe('.constructor', () => {
    it('should return an instance of Coordinate', () => {
      const c = new Coordinate({
        x: 1,
        y: 1,
      });

      c.should.be.instanceof(Coordinate);
    });
  });

  describe('.hash', () => {
    it('should return an hash of Coordinate', () => {
      const c = new Coordinate({
        x: 1,
        y: 1,
      });

      c.hash.should.equal('x1y1');
    });
  });
});
