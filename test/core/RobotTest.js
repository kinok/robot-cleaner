const Robot = require('../../src/core/Robot');
require('should');

describe('Robot', () => {
  describe('.constructor', () => {
    it('should return an instance of Robot', () => {
      const numberOfCommands = 1;
      const startCoordinates = '0 0';
      const commands = ['E 1'];
      const robot = new Robot({
        numberOfCommands,
        startCoordinates,
        commands,
      });
      robot.should.be.instanceof(Robot);
    });
  });

  describe('.start', () => {
    it('should simple move the robot 2 position to east and clean', () => {
      const numberOfCommands = 1;
      const startCoordinates = '0 0';
      const commands = ['E 2'];
      const robot = new Robot({
        numberOfCommands,
        startCoordinates,
        commands,
      });

      // We make the robot moves from its initial position to east 2 positions
      // That make 3 cleaned positions (initial + 2 we moved to).
      robot.start();
      robot.output.should.equal(3);
    });

    it('should move the robot back and forth and clean', () => {
      const numberOfCommands = 2;
      const startCoordinates = '0 0';
      const commands = ['E 2', 'W 2'];
      const robot = new Robot({
        numberOfCommands,
        startCoordinates,
        commands,
      });

      // We make robot move east and west with the same position
      // So the robot won't count already clean position
      robot.start();
      robot.output.should.equal(3);
    });

    it('should move the robot in square 1x1', () => {
      const numberOfCommands = 4;
      const startCoordinates = '0 0';
      const commands = ['E 1', 'N 1', 'W 1', 'S 1'];
      const robot = new Robot({
        numberOfCommands,
        startCoordinates,
        commands,
      });

      // We make robot move in a square
      // So the robot should be cleaning 4 positions
      robot.start();
      robot.output.should.equal(4);
    });

    it('should not move the robot on east boundary', () => {
      const numberOfCommands = 1;
      const startCoordinates = '0 0';
      const commands = ['E 10'];
      const robot = new Robot({
        numberOfCommands,
        startCoordinates,
        commands,
      });

      // We override the maxX boundary
      // Robot should do nothing when it is on east boundary (x axis)
      robot.setBoundaries({ maxX: 3 });
      robot.start();
      robot.output.should.equal(4);
    });

    it('should not move the robot on west boundary', () => {
      const numberOfCommands = 1;
      const startCoordinates = '0 0';
      const commands = ['W 10'];
      const robot = new Robot({
        numberOfCommands,
        startCoordinates,
        commands,
      });

      // We override the maxX boundary
      // Robot should do nothing when it is on east boundary (-x axis)
      robot.setBoundaries({ minX: -3 });
      robot.start();
      robot.output.should.equal(4);
    });

    it('should not move the robot on north boundary', () => {
      const numberOfCommands = 1;
      const startCoordinates = '0 0';
      const commands = ['N 10'];
      const robot = new Robot({
        numberOfCommands,
        startCoordinates,
        commands,
      });

      // We override the maxX boundary
      // Robot should do nothing when it is on east boundary (y axis)
      robot.setBoundaries({ maxY: 3 });
      robot.start();
      robot.output.should.equal(4);
    });

    it('should not move the robot on south boundary', () => {
      const numberOfCommands = 1;
      const startCoordinates = '0 0';
      const commands = ['S 10'];
      const robot = new Robot({
        numberOfCommands,
        startCoordinates,
        commands,
      });

      // We override the maxX boundary
      // Robot should do nothing when it is on east boundary (-y axis)
      robot.setBoundaries({ minY: -3 });
      robot.start();
      robot.output.should.equal(4);
    });

    it('should not move the robot on max command boundary', () => {
      const numberOfCommands = 2;
      const startCoordinates = '0 0';
      const commands = ['N 1', 'N 1'];
      const robot = new Robot({
        numberOfCommands,
        startCoordinates,
        commands,
      });

      // We override the maxX boundary
      // Robot should do nothing when it is on east boundary (x axis)
      robot.setBoundaries({ maxCommand: 1 });
      robot.start();
      robot.output.should.equal(2);
    });
  });
});
